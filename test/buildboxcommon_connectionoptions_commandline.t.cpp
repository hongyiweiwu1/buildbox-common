/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_connectionoptions_commandline.h>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace buildboxcommon;
using namespace testing;

// clang-format off
const char *argvTest[] = {
    "/some/path/to/some_program.tsk",
    "--cas-remote=http://127.0.0.1:50011",
    "--cas-instance=dev",
    "--cas-server-cert=my-server-cert",
    "--cas-client-key=my-client-key",
    "--cas-client-cert=my-client-cert",
    "--cas-access-token=my-access-token",
    "--cas-googleapi-auth=true",
    "--cas-retry-limit=10",
    "--cas-retry-delay=500",
    "--cas-request-timeout=123",
    "--cas-keepalive-time=456",
    "--cas-load-balancing-policy=round_robin"
};

const char *argvTestDefaults[] = {
    "/some/path/to/some_program.tsk",
    "--cas-remote=http://127.0.0.1:50011"
};

const char *argvTestRequired[] = {
    "/some/path/to/some_program.tsk"
};

const char *argvTestConfig[] = {
    "/some/path/to/some_program.tsk",
    "--cas-remote=http://127.0.0.1:50011",
    "--cas-client-cert=my-client-cert",
    "--cas-token-reload-interval=10"
};

const char *argvTestUpdate[] = {
    "/some/path/to/some_program.tsk",
    "--cas-remote=http://127.0.0.1:50021",
    "--cas-client-cert=my-updated-client-cert",
};
// clang-format on

TEST(ConnectionOptionsCommandLineTest, Test)
{
    ConnectionOptionsCommandLine spec("CAS", "cas-");
    CommandLine commandLine(spec.spec());

    EXPECT_TRUE(
        commandLine.parse(sizeof(argvTest) / sizeof(const char *), argvTest));

    EXPECT_FALSE(ConnectionOptionsCommandLine::configureChannel(
        commandLine, "cas-", nullptr));

    ConnectionOptions channel;
    EXPECT_TRUE(ConnectionOptionsCommandLine::configureChannel(
        commandLine, "cas-", &channel));

    EXPECT_EQ("http://127.0.0.1:50011", channel.d_url);
    EXPECT_EQ("dev", channel.d_instanceName);
    EXPECT_EQ("my-server-cert", channel.d_serverCertPath);
    EXPECT_EQ("my-client-key", channel.d_clientKeyPath);
    EXPECT_EQ("my-client-cert", channel.d_clientCertPath);
    EXPECT_EQ("my-access-token", channel.d_accessTokenPath);
    EXPECT_TRUE(channel.d_useGoogleApiAuth);
    EXPECT_EQ("10", channel.d_retryLimit);
    EXPECT_EQ("500", channel.d_retryDelay);
    EXPECT_EQ("123", channel.d_requestTimeout);
    EXPECT_EQ("456", channel.d_keepaliveTime);
    EXPECT_EQ("round_robin", channel.d_loadBalancingPolicy);
}

TEST(ConnectionOptionsCommandLineTest, TestDefaults)
{
    ConnectionOptionsCommandLine spec("CAS", "cas-");
    CommandLine commandLine(spec.spec());

    EXPECT_TRUE(commandLine.parse(
        sizeof(argvTestDefaults) / sizeof(const char *), argvTestDefaults));

    ConnectionOptions channel;
    EXPECT_TRUE(ConnectionOptionsCommandLine::configureChannel(
        commandLine, "cas-", &channel));

    EXPECT_EQ("http://127.0.0.1:50011", channel.d_url);

    // test default values
    EXPECT_EQ("", channel.d_instanceName);
    EXPECT_FALSE(channel.d_useGoogleApiAuth);
    EXPECT_EQ("4", channel.d_retryLimit);
    EXPECT_EQ("1000", channel.d_retryDelay);
    EXPECT_EQ("0", channel.d_requestTimeout);
    EXPECT_EQ("0", channel.d_keepaliveTime);

    // untouched
    EXPECT_EQ(channel.d_serverCertPath, "");
    EXPECT_EQ(channel.d_clientKeyPath, "");
    EXPECT_EQ(channel.d_clientCertPath, "");
    EXPECT_EQ(channel.d_accessTokenPath, "");
}

TEST(ConnectionOptionsCommandLineTest, TestRequired)
{
    ConnectionOptionsCommandLine spec("CAS", "cas-", true);
    CommandLine commandLine(spec.spec());

    EXPECT_FALSE(commandLine.parse(
        sizeof(argvTestRequired) / sizeof(const char *), argvTestRequired));
}

TEST(ConnectionOptionsCommandLineTest, TestUpdate)
{
    ConnectionOptionsCommandLine spec(
        buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-", true));
    CommandLine commandLine(spec.spec());

    EXPECT_TRUE(commandLine.parse(
        sizeof(argvTestConfig) / sizeof(const char *), argvTestConfig));

    ConnectionOptions channel;
    ConnectionOptionsCommandLine::configureChannel(commandLine, "cas-",
                                                   &channel);

    EXPECT_EQ("http://127.0.0.1:50011", channel.d_url);
    EXPECT_EQ("my-client-cert", channel.d_clientCertPath);
    EXPECT_EQ("10", channel.d_tokenReloadInterval);

    ConnectionOptionsCommandLine specUpdate(
        buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-", true));
    CommandLine commandLineUpdate(specUpdate.spec());

    commandLineUpdate.parse(sizeof(argvTestUpdate) / sizeof(const char *),
                            argvTestUpdate);
    ConnectionOptionsCommandLine::updateChannelOptions(commandLineUpdate,
                                                       "cas-", &channel);

    // Two updated values and one unchanged
    EXPECT_EQ("http://127.0.0.1:50021", channel.d_url);
    EXPECT_EQ("my-updated-client-cert", channel.d_clientCertPath);
    EXPECT_EQ("10", channel.d_tokenReloadInterval);
}
