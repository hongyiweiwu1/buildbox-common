/*
 * Copyright 2018-2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCOMMON_GRPCCLIENT
#define INCLUDED_BUILDBOXCOMMON_GRPCCLIENT

#include <functional>
#include <memory>

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_grpcerror.h>
#include <buildboxcommon_grpcretrier.h>
#include <buildboxcommon_protos.h>
#include <buildboxcommon_requestmetadata.h>

namespace buildboxcommon {

/**
 * Implements a mechanism to communicate with gRPC servers.
 */
class GrpcClient {
  public:
    GrpcClient() {}

    /**
     * Connect to the gRPC endpoint with the given connection options.
     */
    void init(const ConnectionOptions &options);

    std::shared_ptr<grpc::Channel> channel() { return d_channel; }

    void setToolDetails(const std::string &tool_name,
                        const std::string &tool_version);
    /**
     * Set the optional ID values to be attached to requests.
     */
    void setRequestMetadata(const std::string &action_id,
                            const std::string &tool_invocation_id,
                            const std::string &correlated_invocations_id);

    /* Passing this optional object to methods allows callers to read stats
     * about the gRPC request.
     */
    struct RequestStats {
        RequestStats() : d_grpcRetryCount(0){};

        // Number of gRPC retries (requests after the initial one) issued
        // until the call suceeded, failed with a non-retryable error code, or
        // the retry limit was exceeded.
        unsigned int d_grpcRetryCount;
    };

    std::string instanceName() const;

    void setInstanceName(const std::string &instance_name);

    void
    issueRequest(const buildboxcommon::GrpcRetrier::GrpcInvocation &invocation,
                 const std::string &invocationName,
                 RequestStats *requestStats) const;

    void
    issueRequest(const buildboxcommon::GrpcRetrier::GrpcInvocation &invocation,
                 const std::string &invocationName,
                 const std::chrono::seconds &requestTimeout,
                 RequestStats *requestStats) const;

    GrpcRetrier makeRetrier(const GrpcRetrier::GrpcInvocation &invocation,
                            const std::string &name,
                            const std::chrono::seconds &requestTimeout =
                                std::chrono::seconds::zero()) const;

    int retryLimit() const;

    std::chrono::seconds requestTimeout() const;

    void setRetryLimit(int limit);

    void setRequestTimeout(const std::chrono::seconds &requestTimeout);

  private:
    // initialized here to prevent errors, in case options are not passed into
    // init
    int d_grpcRetryLimit = 0;
    int d_grpcRetryDelay = 100;
    std::chrono::seconds d_grpcRequestTimeout = std::chrono::seconds::zero();

    std::shared_ptr<grpc::Channel> d_channel;
    std::string d_instanceName;

    RequestMetadataGenerator d_metadata_generator;
    const std::function<void(grpc::ClientContext *)>
        d_metadata_attach_function = [&](grpc::ClientContext *context) {
            d_metadata_generator.attach_request_metadata(context);
        };

    /*
     * RequestMetadata values. They will be attached to requests sent by this
     * client.
     */
    ToolDetails d_tool_details;
    std::string d_action_id;
    std::string d_tool_invocation_id;
    std::string d_correlated_invocations_id;
};

} // namespace buildboxcommon

#endif
